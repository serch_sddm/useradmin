package com.iwa.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
 
import com.iwa.model.Profile;

@Repository("ProfileDao")
public class ProfileDaoImpl extends AbstractDao<Integer, Profile>implements ProfileDao {

    public Profile findById(int id) {
        return getByKey(id);
    }
 
    public Profile findByKeyType(String keyType) {
        Criteria crit = createEntityCriteria();
        crit.add(Restrictions.eq("keyType", keyType));
        return (Profile) crit.uniqueResult();
    }
     
    @SuppressWarnings("unchecked")
    public List<Profile> findAll(){
        Criteria crit = createEntityCriteria();
        crit.addOrder(Order.asc("keyType"));
        return (List<Profile>)crit.list();
    }
    
}
