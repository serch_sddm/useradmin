package com.iwa.dao;

import java.util.List;

import com.iwa.model.Profile;

public interface ProfileDao {

	List<Profile> findAll();

	Profile findByKeyType(String keyType);

	Profile findById(int id);
}
