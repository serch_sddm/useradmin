package com.iwa.service;

import java.util.List;

import com.iwa.model.User;

public interface UserService {

	User findById(int id);

	User findByUsername(String username);

	void saveUser(User user);

	void updateUser(User user);

	void deleteUserByUsername(String username);

	List<User> findAllUsers();
	List<User> findAllUsers(Long page);

	boolean isUserUsernameUnique(Integer id, String username);
}
