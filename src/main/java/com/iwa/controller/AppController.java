package com.iwa.controller;

import java.util.List;
import java.util.Locale;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.iwa.model.User;
import com.iwa.model.Profile;
import com.iwa.service.ProfileService;
import com.iwa.service.UserService;

@Controller
@RequestMapping("/")
@SessionAttributes("roles")
public class AppController {

	@Autowired
	UserService userService;

	@Autowired
	ProfileService profileService;

	@Autowired
	MessageSource messageSource;

	/**
	 * This method will list all existing users.
	 */
	@RequestMapping(value = { "/", "/list" }, method = RequestMethod.GET)
	//@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String listUsers(@RequestParam(value = "page", required = false) Long page, ModelMap model) {

		
		int startpage = (int) (page - 5 > 0?page - 5:1);
	    int endpage = startpage + 10;
	      
		//List<User> users = userService.findAllUsers();
		List<User> users = userService.findAllUsers(page);
		model.addAttribute("users", users);
		
		model.addAttribute("startpage",startpage);
		model.addAttribute("endpage",endpage);
		
		return "userslist";
	}

	/**
	 * This method will provide the medium to add a new user.
	 */
	@RequestMapping(value = { "/newuser" }, method = RequestMethod.GET)
	public String newUser(ModelMap model) {
		User user = new User();
		model.addAttribute("user", user);
		model.addAttribute("edit", false);
		return "registration";
	}

	/**
	 * This method will be called on form submission, handling POST request for
	 * saving user in database. It also validates the user input
	 */
	@RequestMapping(value = { "/newuser" }, method = RequestMethod.POST)
    public String saveUser(@Valid User user, BindingResult result,
            ModelMap model) {
 
        if (result.hasErrors()) {
            return "registration";
        }
 
        /*
         * Preferred way to achieve uniqueness of field [username] should be implementing custom @Unique annotation 
         * and applying it on field [username] of Model class [User].
         * 
         * Below mentioned peace of code [if block] is to demonstrate that you can fill custom errors outside the 
 
validation
         * framework as well while still using internationalized messages.
         * 
         */
        if(!userService.isUserUsernameUnique(user.getId(), user.getUsername())){
            FieldError usernameError =new FieldError("user","username",messageSource.getMessage("non.unique.username", new
 
String[]{user.getUsername()}, Locale.getDefault()));
            result.addError(usernameError);
            return "registration";
        }
         
        userService.saveUser(user);
 
        model.addAttribute("success", "User " + user.getFirstName() + " "+ user.getLastName() + " registered successfully");
        //return "success";
        return "registrationsuccess";
    }

	/**
	 * This method will provide the medium to update an existing user.
	 */
	@RequestMapping(value = { "/edit-user-{username}" }, method = RequestMethod.GET)
	public String editUser(@PathVariable String username, ModelMap model) {
		User user = userService.findByUsername(username);
		model.addAttribute("user", user);
		model.addAttribute("edit", true);
		return "registration";
	}

	/**
	 * This method will be called on form submission, handling POST request for
	 * updating user in database. It also validates the user input
	 */
	@RequestMapping(value = { "/edit-user-{username}" }, method = RequestMethod.POST)
	public String updateUser(@Valid User user, BindingResult result,
			ModelMap model, @PathVariable String username) {

		if (result.hasErrors()) {
			return "registration";
		}

		/*
		 * //Uncomment below 'if block' if you WANT TO ALLOW UPDATING USERNAME in
		 * UI which is a unique key to a User.
		 * if(!userService.isUserUsernameUnique(user.getId(), user.getUsername())){
		 * FieldError ssoError =new
		 * FieldError("user","username",messageSource.getMessage
		 * ("non.unique.username", new
		 * 
		 * String[]{user.getUsername()}, Locale.getDefault()));
		 * result.addError(ssoError); return "registration"; }
		 */

		userService.updateUser(user);

		model.addAttribute("success", "User " + user.getFirstName() + " "
				+ user.getLastName() + " updated successfully");
		return "registrationsuccess";
	}

	/**
	 * This method will delete an user by it's USERNAME value.
	 */
	@RequestMapping(value = { "/delete-user-{username}" }, method = RequestMethod.GET)
	public String deleteUser(@PathVariable String username) {
		userService.deleteUserByUsername(username);
		return "redirect:/list/?page=1";
	}

	/**
	 * This method will provide UserProfile list to views
	 */
	@ModelAttribute("roles")
	public List<Profile> initializeProfiles() {
		return profileService.findAll();
	}

}
